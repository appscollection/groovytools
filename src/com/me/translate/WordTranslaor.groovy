package com.me.translate;

import app.ngenhttplib.http.HttpInvoker
import java.util.Map.Entry

def trn = new WordTranslator();
//trn.initTranslatorJob();
trn.initFromTranslatedFile();

class WordTranslator{

	private String outputFolder = "output";
	private String wordFile = "tokens.txt";
	private String translatedFile = "translated_tokens";
	private String finalFile = "translated_words.csv";

	private SortedSet<String> sortedSet = new TreeSet<String>();
	private SortedMap<String, String> translatedMap = new TreeMap<String, String>();

	//for storing words list as a sheet
	//For optimal print view (A4, Landscape, Fit to width)
	private int noOfRows = 50;//min:1
	private int noOfSheet = 1;//optional, min value: 1
	private delimeter = ",";

	private String googleApiUrl = "https://www.googleapis.com/language/translate/v2?key=AIzaSyBtfSp9TSlUDCNJ0jTwFc-PelOc24-LuzM&source=en&target=bn&q=";


	public setInputFile(String file){
		this.wordFile = file;
	}

	public void setTranslateFile(String file){
		this.translatedFile = file;
	}

	def initTranslatorJob(){
		sortWords();
		initGoogleTranslate();
		formatFinalData();
	}

	def initFromTranslatedFile(){
		File file = new File(outputFolder+"/"+translatedFile);
		String txt = file.getText("UTF-8");
		txt.split(/\s*\n+\s*/).each{
			it= it.trim().split(":");
			if(it.size() == 2){
				translatedMap.put(it[0],it[1]);
			}
		}
		formatFinalData();
	}

	def formatFinalData(){
		File file = new File(outputFolder+"/"+finalFile);
		if(!file.exists()){
			file.createNewFile();
		}else{
			file.delete();
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file);

		if(noOfSheet<1){
			noOfSheet = 1;
		}
		int size = translatedMap.size();
		int perSheet = Math.ceil(size/(double)noOfSheet);

		int sheetPtr = 0;
		String[] rows = new String[noOfRows];
		int colPtr = 0;

		for(Entry<String, HashMap> entry : translatedMap.entrySet()){
			def k = entry.getKey();
			def v = entry.getValue();

			if(sheetPtr < perSheet){
				println entry
				if(colPtr < noOfRows){
					if(!rows[colPtr]){
						rows[colPtr] = k+delimeter+v;
					}else{
						rows[colPtr] = rows[colPtr]+delimeter+k+delimeter+v;
					}
					colPtr++;
				}else{
					rows[0] = rows[0]+delimeter+k+delimeter+v;
					colPtr = 1;
				}
				sheetPtr++;
			}else{
				rows.each{val->
					fw.append(val+'\n');
				}
				fw.append('\n');
				sheetPtr = 0;
				colPtr = 0;
				rows = new String[noOfRows];
			}
		}
		rows.each{val->
			fw.append(val+'\n');
		}
		fw.close();
	}

	def initGoogleTranslate(){
		HttpInvoker hi = new HttpInvoker();

		translatedFile = translatedFile+"_"+new Date().getTime();
		File file = new File(outputFolder+"/"+translatedFile);
		if(!file.exists()){
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file);

		for(String it: sortedSet){
			hi.setUrl(googleApiUrl+it.replaceAll(/\s/,'%20'));
			def json = hi.getStringData();
			def val = (json =~ /translatedText"[^"]+.([^"]+)/)[0][1]
			translatedMap.put(it,val)
			val = it+":"+val
			fw.append(val+"\n")
			println val
		}
		fw.close();
	}

	private void sortWords(){
		File file = new File(outputFolder+"/"+wordFile);
		String txt = file.getText("UTF-8");
		txt.split(/\s*\n+\s*/).each{
			it= it.trim();
			if(it){
				sortedSet.add(it);
			}
		}
	}
}
